# HDRP Tests Project



## Main References

- PBR on HDRP: https://www.youtube.com/watch?v=zd7E2i8zgCE
- Glass material on HDRP: https://www.youtube.com/watch?v=E31H5WuA-d4
- PBR material using shader: https://learn.unity.com/tutorial/shadergraph-pbr-material#
- Motion blur: https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@6.9/manual/Post-Processing-Motion-Blur.html 
- Motion vectors in HDRP: https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@6.9/manual/Motion-Vectors.html
- Photorealistic materials in unity: https://www.youtube.com/watch?v=_LaVvGlkBDs


```
cd existing_repo
git remote add origin https://gitlab.com/gabriel.goncalves.borges/hdrp-tests-project.git
git branch -M main
git push -uf origin main
```


